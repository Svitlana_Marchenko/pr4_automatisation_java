package org.example;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class contains unit tests.
 */
public class MenuTest {

    /**
     * Tests the {@link Menu#printAllTraining(List)} method.
     * Verifies that the method returns the expected output based on the given list of training sessions.
     *
     * @see Menu#printAllTraining(List)
     */
    @Test
    public void testPrintAllTraining() {
        // Test setup
        List<TrainingClass> list = new ArrayList<>();
        list.add(new TrainingClass(TrainingClass.Type.Running, 30, 200));
        list.add(new TrainingClass(TrainingClass.Type.Dancing, 45, 300));
        String expectedOutput = "Training Report\n"
                + "---------------------------\n"
                + "1) Training Type: Running\n"
                + "Duration: 30 min\n"
                + "Calories: 200 cal\n"
                + "---------------------------\n"
                + "2) Training Type: Dancing\n"
                + "Duration: 45 min\n"
                + "Calories: 300 cal\n"
                + "---------------------------";

        // Test execution
        String actualOutput = Menu.printAllTraining(list);

        // Test verification
        assertEquals(expectedOutput, actualOutput);
    }

    /**
     * Tests the {@link Statistic#countMinutes(List)} method.
     * Verifies that the method returns the correct total duration of the given list of training sessions.
     *
     * @see Statistic#countMinutes(List)
     */
    @Test
    public void testCountMinutes() {
        // Test setup
        List<TrainingClass> list = new ArrayList<>();
        list.add(new TrainingClass(TrainingClass.Type.Running, 30, 200));
        list.add(new TrainingClass(TrainingClass.Type.Dancing, 45, 300));

        // Test execution
        int actualMinutes = Statistic.countMinutes(list);

        // Test verification
        assertEquals(75, actualMinutes);
    }

    /**
     * Tests the {@link Statistic#countCalories(List)} method.
     * Verifies that the method returns the correct total calories burned of the given list of training sessions.
     *
     * @see Statistic#countCalories(List)
     */
    @Test
    public void testCountCalories() {
        // Test setup
        List<TrainingClass> list = new ArrayList<>();
        list.add(new TrainingClass(TrainingClass.Type.Running, 30, 200));
        list.add(new TrainingClass(TrainingClass.Type.Dancing, 45, 300));

        // Test execution
        int actualCalories = Statistic.countCalories(list);

        // Test verification
        assertEquals(500, actualCalories);
    }
}
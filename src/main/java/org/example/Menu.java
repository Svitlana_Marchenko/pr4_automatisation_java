package org.example;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The {@code Menu} class represents a menu system for managing training sessions.
 */
public class Menu {
    private static final String RESFILE = "save.txt";
    private static final String DIRRES = "C:\\pr3_gradle\\results";

    static List<TrainingClass> list;

    /**
     * The entry point of the application.
     *
     * @param args The command-line arguments.
     */
    public static void main(String[] args) {
        start();
    }

    private static void start() {
        if (checkIfExist(DIRRES, RESFILE))
            list = restore(new File(DIRRES, RESFILE));
        else
            list = new ArrayList<>();
        Scanner sca = new Scanner(System.in);
        mainMenu(sca);
    }

    private static void mainMenu(Scanner sc) {
        System.out.println("What would you like to do: 1 - add training, 2 - show history, 3 - statistic, 4 - exit ");
        String s = sc.nextLine();
        while (!checkNumMenu(s, 1, 4)) {
            System.out.println("What would you like to do: 1 - add training, 2 - show history, 3 - statistic, 4 - exit ");
            s = sc.nextLine();
        }
        int choice = Integer.parseInt(s);
        switch (choice) {
            case 1:
                addNewTraining(sc);
                break;
            case 2:
                allTraining(sc);
                break;
            case 3:
                mainStatisticMethod(sc);
                break;
            case 4:
                save(list, new File(DIRRES, RESFILE));
                sc.close();
                System.exit(0);
                break;
            default:
                System.out.println("Invalid choice. Please try again.");
                mainMenu(sc);
        }
    }

    private static void allTraining(Scanner sc) {
        System.out.println(printAllTraining(list));
        mainMenu(sc);
    }

    private static void mainStatisticMethod(Scanner sc) {
        System.out.println("Calories = " + Statistic.countCalories(list));
        System.out.println("Duration = " + Statistic.countMinutes(list));
        mainMenu(sc);
    }

    /**
     * Generates a report of all training sessions.
     *
     * @param l The list of training sessions.
     * @return The generated report.
     */
    public static String printAllTraining(List<TrainingClass> l) {
        String a = "";
        a += "Training Report";
        a += "\n---------------------------";
        int counter = 1;
        for (TrainingClass training : l) {
            a += ("\n" + counter + ") " + "Training Type: " + training.getType());
            a += ("\nDuration: " + training.getDuration() + " min");
            a += ("\nCalories: " + training.getCalories() + " cal");
            a += ("\n---------------------------");
            counter++;
        }
        return a;
    }

    private static void addNewTraining(Scanner sc) {
        String s;
        System.out.println("Choose the type of training: ");
        System.out.println(printAllTrainingsType());
        s = sc.nextLine();

        TrainingClass.Type t = null;
        switch (Integer.parseInt(s)) {
            case 1:
                t = TrainingClass.Type.Running;
                break;
            case 2:
                t = TrainingClass.Type.Dancing;
                break;
            case 3:
                t = TrainingClass.Type.Weightlifting;
                break;
        }

        System.out.println("Duration (in minutes): ");
        String dur = sc.nextLine();
        System.out.println("Calories: ");
        String cal = sc.nextLine();

        list.add(new TrainingClass(t, Integer.parseInt(dur), Integer.parseInt(cal)));
        System.out.println("New training was added");
        Menu.mainMenu(sc);
    }

    private static String printAllTrainingsType() {
        String s = "";
        int counter = 1;
        for (TrainingClass.Type t : TrainingClass.Type.values()) {
            s += counter + ") " + t + "\n";
            counter++;
        }
        return s;
    }

    private static boolean checkNumMenu(String s, int i, int i1) {
        return (Integer.parseInt(s) >= i && Integer.parseInt(s) <= i1);
    }

    /**
     * Checks if a file exists in the specified directory.
     *
     * @param pathDir The directory path.
     * @param name    The file name.
     * @return {@code true} if the file exists, {@code false} otherwise.
     */
    public static boolean checkIfExist(String pathDir, String name) {
        File file = new File(pathDir, name);
        return file.exists() && file.isFile() && file.length() > 0;
    }

    /**
     * Saves the list of training sessions to a file.
     *
     * @param list The list of training sessions.
     * @param f    The file to save the list to.
     */
    public static void save(List<TrainingClass> list, File f) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(f);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(list);
            objectOutputStream.close();
            fileOutputStream.close();
            System.out.println("ArrayList saved successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restores the list of training sessions from a file.
     *
     * @param f The file to restore the list from.
     * @return The restored list of training sessions.
     */
    public static List<TrainingClass> restore(File f) {
        List<TrainingClass> restoredList = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(f);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            restoredList = (List<TrainingClass>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            System.out.println("ArrayList restored successfully.");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return restoredList;
    }
}

package org.example;

import java.io.Serializable;

/**
 * The {@code TrainingClass} class represents a training session.
 */
public class TrainingClass implements Serializable {
    /**
     * The type of training.
     */
    public enum Type {
        Running,
        Dancing,
        Weightlifting
    }

    private Type type;
    private int duration;
    private int calories;

    /**
     * Constructs a new {@code TrainingClass} instance with the specified type, duration, and calories.
     *
     * @param type     The type of training.
     * @param duration The duration of the training in minutes.
     * @param calories The number of calories burned during the training.
     */
    public TrainingClass(final Type type, final int duration, final int calories) {
        this.type = type;
        this.duration = duration;
        this.calories = calories;
    }

    /**
     * Returns the type of training.
     *
     * @return The type of training.
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the duration of the training in minutes.
     *
     * @return The duration of the training in minutes.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Returns the number of calories burned during the training.
     *
     * @return The number of calories burned during the training.
     */
    public int getCalories() {
        return calories;
    }

    /**
     * Returns a string representation of the {@code TrainingClass} instance.
     *
     * @return A string representation of the {@code TrainingClass} instance.
     */
    @Override
    public String toString() {
        return "TrainingClass{" +
                "type=" + type +
                ", duration=" + duration +
                ", calories=" + calories +
                '}';
    }
}

package org.example;

import java.util.List;

/**
 * The {@code Statistic} class provides statistical calculations for training sessions.
 */
public class Statistic {

    /**
     * Calculates the total duration of the training sessions in minutes.
     *
     * @param list The list of training sessions.
     * @return The total duration of the training sessions in minutes.
     */
    public static int countMinutes(final List<TrainingClass> list) {
        int totalDurationT = 0;
        for (TrainingClass training : list) {
            totalDurationT += training.getDuration();
        }
        return totalDurationT;
    }

    /**
     * Calculates the total calories burned during the training sessions.
     *
     * @param list The list of training sessions.
     * @return The total calories burned during the training sessions.
     */
    public static int countCalories(final List<TrainingClass> list) {
        int totalCalories = 0;
        for (TrainingClass training : list) {
            totalCalories += training.getCalories();
        }
        return totalCalories;
    }
}
